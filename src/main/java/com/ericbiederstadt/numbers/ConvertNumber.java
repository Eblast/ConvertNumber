package com.ericbiederstadt.numbers;

public class ConvertNumber {

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Please enter a number between -999999 to 999999");
            return;
        }

        NumberPrinter printer = new NumberPrinter();
        Integer inputNumber;
        try{
         inputNumber =  Integer.valueOf(args[0]);
        }catch (NumberFormatException e){
            System.out.println("Please enter a number between -999999 to 999999");
            return;
        }


        System.out.println(printer.printNumber(inputNumber));
    }
}
