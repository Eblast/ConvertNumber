package com.ericbiederstadt.numbers;

public class NumberPrinter {
    private String[] lessThan20 = {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private String[] tensDigits = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

    public String printNumber(Integer number) {
        if (number == 0) {
            return "zero";
        } else if (number < -999999 || number > 999999) {
            return "Please enter a number between -999999 to 999999";
        }

        Integer absNumber = Math.abs(number);
        int thousands = absNumber / 1000;
        String thousandsString = thousands > 0 ? convertNumberLessThanOneThousand(thousands) + " thousand " : "";

        int remainder = absNumber % 1000;
        String lessThanThousandsString = convertNumberLessThanOneThousand(remainder);

        return (number < 0 ? "negative " : "") + thousandsString + lessThanThousandsString;
    }

    private String convertNumberLessThanOneThousand(Integer number) {
        int hundredsPlace = number / 100;
        int hundredsRemainder = number % 100;
        String hundredsString = "";
        if (hundredsPlace > 0) {
            hundredsString = lessThan20[hundredsPlace] + " hundred ";
        }

        return hundredsString + convertNumberLessThanOneHundred(hundredsRemainder);
    }

    private String convertNumberLessThanOneHundred(Integer number) {
        if (number < 20) {
            return lessThan20[number];
        } else {
            Integer tensPlace = number / 10;
            Integer onesPlace = number % 10;
            return tensDigits[tensPlace] + (onesPlace > 0 ? " " + lessThan20[onesPlace] : "");
        }
    }
}